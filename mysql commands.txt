
NOTE: you can use this when, .sql has create database query,if not use mysqldump
#mysql -uroot -pcloudera < mysqlsampledatabase.sql

using mysqldump, when the creation of database commonand not present in .sql file
#mysql -uroot -pcloudera
mysql> create database classicmodels;
mysql> quit;
#mysqldump -uroot -pcloudera classicmodels < classicmodelsdb.sql

MYSQL import of CSV data
http://www.mysqltutorial.org/import-csv-file-mysql-table/
HINT: Use mysqlimport command
#mysqlimport -uusername -ppassword dbname csv/.txt filename
NOTE: DB and table need to created accordingly before using mysqlimport

mysql> create database realestate;
mysql> use realestate;
mysql> create table realestatedetails(id int,datereg varchar(10), sqft varchar(20),price int,address varchar(25));
(or)
mysql> create table realestatedetails(id int,datereg varchar(10), sqft double(20,2),price int,address varchar(25));
(or)
mysql> create table realestatedetails(id int,datereg date, sqft double(20,2),price int,address varchar(25));

mysql> LOAD DATA LOCAL INFILE 'realestate.txt' INTO TABLE realestatedetails FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'

select * from realestatedetails;


Delimiters

For specify delimiters used below
mysqldump -u [username] -p -t -T/path/to/directory [database] --fields-terminated-by=,


HINT: For fields enclosed with double quotes: "1","foo1","bar","2007-12-15 04:20:43",use below command
mysqldump -u [username] -p -t -T/path/to/directory [database] --fields-enclosed-by=\" --fields-terminated-by=,



