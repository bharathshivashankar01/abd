listings = LOAD '/user/cloudera/realestate/realestate.txt' USING PigStorage(',') as (listing_id:int, date:chararray, list_price:float,sq_feet:int, address:chararray);

--DUMP listings;

listings = FOREACH listings GENERATE listing_id,ToDate(date, 'YYYY-MM-dd') AS date,list_price,sq_feet,address;

DUMP listings;

bighomes = FILTER listings BY sq_feet >= 2000;

DUMP bighomes;

bighomes_dateprice = FOREACH bighomes GENERATE listing_id,date,list_price;

DUMP bighomes_dateprice;

STORE bighomes_dateprice INTO '/user/cloudera/realestateoutput';

